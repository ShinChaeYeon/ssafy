# -*- coding: utf-8 -*-
import re
import urllib.request
from random import *
from bs4 import BeautifulSoup
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter

 
SLACK_TOKEN = "xoxb-688880529351-689741885621-uJZSi7oXXvlbZFqE8OhKtSJQ"
SLACK_SIGNING_SECRET = "333757a2b2063ff9a86c9c7c59d08f2e"

app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

no1 = ImageBlock(
    image_url = "http://blogfiles.naver.net/20141026_188/caccat_1414253367193bHXOD_JPEG/IMG_20141009_063520.jpg",
    alt_text = "ERROR IMAGE"
)
no2 = ImageBlock(
    image_url = "https://blogfiles.pstatic.net/MjAxOTA3MTFfMTQ0/MDAxNTYyODIyNjY2MjUy.vRRc-Q-wwa-hK4j-WKSHLWojW1si1M0cWqh5tOHsr3Mg.Ky9264ajWc5rB_MXFyJ0eplk3VxD2LWSkExU_R9JCIgg.JPEG.bu_za/1.jpg",
    alt_text = "ERROR IMAGE"
)
no3 = ImageBlock(
    image_url = "http://blogfiles.naver.net/MjAxNzA1MTRfMjY2/MDAxNDk0NzU1ODk3MDk4.sY_qAIx458dX5GdbFC7COFbcGgnjx8X1CDMxQj080twg.GkNoWmK2gD9irBkN0cdud23XLUEukw3bElQj17otATkg.JPEG.kis6539/JTBC_%BE%C6%B4%C2_%C7%FC%B4%D4.E75.170513.720p-NEXT_0001490664ms.jpg",
    alt_text = "ERROR IMAGE"
)
no4 = ImageBlock(
    image_url = "https://pbs.twimg.com/media/CaLk1-kUUAAGDf0.jpg",
    alt_text = "ERROR IMAGE"
)
no5 = ImageBlock(
    image_url = "http://2runzzal.com/media/WkIyL29qVkEvNHhtU0hUaW1zTmtHUT09/thum.png",
    alt_text = "ERROR IMAGE"
)
no6 = ImageBlock(
    image_url = "http://upload.inven.co.kr/upload/2013/04/10/bbs/i1024748329.jpg",
    alt_text = "ERROR IMAGE"
)
no7 = ImageBlock(
    image_url = "https://pbs.twimg.com/media/CVJWNRHVAAAYhHY.jpg",
    alt_text = "ERROR IMAGE"
)
no8 = ImageBlock(
    image_url = "https://blogfiles.pstatic.net/MjAxOTA3MTFfNjYg/MDAxNTYyODIyNjk3NTUz.paWg282-4LegBMvGxbRWoZ1n34O97Chiqdcle9Iiabgg.ZKsgt27xlpMVcI5KPhkJSUhyyqKwUB9fYB7CBISPkpQg.JPEG.bu_za/4.jpg",
    alt_text = "ERROR IMAGE" #싫은데
)
no9 = ImageBlock(
    image_url = "https://blogfiles.pstatic.net/MjAxOTA3MTFfMjUx/MDAxNTYyODIyNzAwNDc4.0rmy3Eq-76ld7RqqskFPf1dkRDe2Mi9iVD1kPOXcKcMg.2KJo6WFGlRxfcStXf0GqDeP8bQQ1McV7aD-lBEO9Nnsg.PNG.bu_za/5.png",
    alt_text = "ERROR IMAGE" #nononono
)
no10 = ImageBlock(
    image_url = "https://blogfiles.pstatic.net/MjAxOTA3MTFfNTUg/MDAxNTYyODIyNzA3NDM2.R_eaE2aDcYxqBr3bLEuuWsDGEoPEkgrKW21mkxP9ktUg.mHCdsClzEJ9y2f8ihSD6kJqH9v5lvTSucjo50pxSt2gg.JPEG.bu_za/7.jpg",
    alt_text = "ERROR IMAGE" #놉
)
no11 = ImageBlock(
    image_url = "http://ssproxy.ucloudbiz.olleh.com/v1/AUTH_6a92e249-183a-47ef-870b-b6f2fb771cfa/gae9/trend/13e2c1b2c4768fcd.orig",
    alt_text = "ERROR IMAGE" #안돼 안바꿔줘 들어가
)
why1 = ImageBlock(
    image_url = "https://postfiles.pstatic.net/MjAxOTA3MTFfMTkx/MDAxNTYyODIyMDMwMjI2.8X0mAGcxg5NFD3_5PNZ8ldWzNoq7etu0fht8i13vYA0g.s_OqH7KzSjMeY1guU0mBsooyEoqJ70-6n2ajAsnEDAYg.PNG.asia924/1.png?type=w966",
    alt_text = "ERROR IMAGE" #굳이
)
why2 = ImageBlock(
    image_url = "https://postfiles.pstatic.net/MjAxOTA3MTFfMTkx/MDAxNTYyODIyMDMwMjMw.T3V-U_Ceot1quXlWse0-IlSdUg674LCEbsFVrLj47wAg.15y3hYPHxHFkNyrTknjFM5-ScoORT0qQB_fXb-KR6_kg.PNG.asia924/2.png?type=w966",
    alt_text = "ERROR IMAGE" #왜
)
why3 = ImageBlock(
    image_url = "https://postfiles.pstatic.net/MjAxOTA3MTFfMTA5/MDAxNTYyODIyMDMwMjMw.wOyGmG5Tb2Gb-5Q8h7OH_TgVorGPQUs-bvnoIRs72X8g.ErOe5Ypu4WogWCzFxCuQCk1bAgrG2P7UPcvzE_8iy10g.PNG.asia924/3.png?type=w966",
    alt_text = "ERROR IMAGE" #나한테?
)
why3 = ImageBlock(
    image_url = "https://postfiles.pstatic.net/MjAxOTA3MTFfMjI5/MDAxNTYyODIyMDMwMjMw.07VJOJeoGSfK_4TAVT8o7u-0-vV1csj2DPboDitcc_sg.247VeIK88Gr4l3Eidkvth9ZyNMjx7qVlB3FjvINNyAIg.PNG.asia924/4.png?type=w966",
    alt_text = "ERROR IMAGE" #나랑?
)
why4 = ImageBlock(
    image_url = "https://blogfiles.pstatic.net/MjAxOTA3MTFfMjg3/MDAxNTYyODIyNjgyMDMz.sd8G3fwwFIE86X8X4gsbxufq0zEKhb-9Ys6XfspMlz8g.lsPTg5kQBfmGZ29TumLixcz5r41rwl5Ysvf3yIaORqEg.JPEG.bu_za/3.jpg",
    alt_text = "ERROR IMAGE" #제가왜요
)
not_my_style1 = ImageBlock(
    image_url = "https://blogfiles.pstatic.net/MjAxOTA3MTFfMjM0/MDAxNTYyODIyNjc1MjA3.uSjXWfNmLONTAe1MJ516FR7YL_jfvZnaOEXPIoLDjpQg.mnyj7lMyEdNvuMHvarMz2J1sMC1SUYE955UQkuACgXMg.PNG.bu_za/2.png",
    alt_text = "ERROR IMAGE" #제스타일아니세요
)
i_dont_know1 = ImageBlock(
    image_url = "https://blogfiles.pstatic.net/MjAxOTA3MTFfMjk2/MDAxNTYyODI3NTQyODcx.QkJrTCQ8gAKIH7D7S3O23_WiZJtKZmjhoeNEha5JVlkg.KNycrKuiP0enMwfmUUp7g8zFHZW48gQtS0xo1IWEMfwg.PNG.wow_v_v/%EC%BA%A1%EC%B2%98.PNG",
    alt_text = "ERROR IMAGE" #초록창
)
i_dont_know2 = ImageBlock(
    image_url = "https://blogfiles.pstatic.net/MjAxOTA3MTFfMTc4/MDAxNTYyODI4MDE2NDky.lXPUszHZSDIIZY5RXNgSqYqCzgCchhjbxzI_nHG0C7wg.u_YOfgMnRv-ZcdH7X0-nLfDL5J2LAhWZR6t7rqrnjTAg.JPEG.wow_v_v/L20160207.99002165453i1.jpg",
    alt_text = "ERROR IMAGE" #초록창
)
go_away1 = ImageBlock(
    image_url = "https://blogfiles.pstatic.net/MjAxOTA3MTFfMjAw/MDAxNTYyODIyNzAzODEz.CFPePLCX1upmqxW0a1YTmrKaNdLXZHYflsTviMKkY8cg.yBDyYXPOs66qVZBGDj2A6Cw4GkDmOQrF5XJsUoKbWTAg.JPEG.bu_za/6.jpg",
    alt_text = "ERROR IMAGE" #꺼져라
)

not_my_style_blocks = [not_my_style1]
go_away_blocks = [go_away1]
why_blocks = [why1, why2, why3, why4]
no_blocks = [no1, no2, no3, no4, no5, no6, no7, no8, no9, no10, no11]
i_dont_know_blocks = [i_dont_know1, i_dont_know2]
photo_block = []
# 크롤링 함수 구현하기

def _crawl_naver_keywords(text, user):
    # 여기에 함수를 구현해봅시다.
    url =  "http://welfoodstory.azurewebsites.net/?category=2%EC%BA%A0%ED%8D%BC%EC%8A%A4-3"
    #url_match = re.search(r'<(http.*?)(\|.*?)?>', url)
    #if not url_match:
    #    return '올바른 URL을 입력해주세요.'

    #url = url_match.group(1)
    sourcecode = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(sourcecode, "html.parser")
    keywords = []
    keywords.append("*<@" + user + ">님 안녕하세요.*\n")
    for menu in soup.find_all("div", class_="menu-item-contents") :
       
        img = menu.find("img")
        img_src = img.get("src")
       
        if not "Content/Images/" in img_src :
            keywords.append(menu.get_text())
            photo = ImageBlock(
               image_url = img_src,
               alt_text = "ERROR IMAGE"
            )
            photo_block.append(photo)
        
    
    # 키워드 리스트를 문자열로 만듭니다.
    return '\n'.join(keywords)


# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    user = event_data["event"]["user"]
    tmpblock = []
    if "메뉴" in text :
        keywords = _crawl_naver_keywords(text, user)
        slack_web_client.chat_postMessage(
            channel=channel,
            text=keywords,
            blocks = extract_json(photo_block)
        )
        return 
    if "사줘" in text :
        i = randint(0,3)
        tmpblock = [why_blocks[i]]
    elif "해줘" in text :
        i = randint(0,3)
        tmpblock = [why_blocks[i]]
    elif "추천" in text :
        i = 0
        tmpblock = [go_away_blocks[i]]    
    elif "사귀자" in text :
        i = 0
        tmpblock = [not_my_style_blocks[i]]
    elif "사랑해" in text :
        i = 0
        tmpblock = [not_my_style_blocks[i]]
    elif "연애하자" in text :
        i = 0
        tmpblock = [not_my_style_blocks[i]]
    elif "뭐지" in text :
        i = randint(0,1)
        tmpblock = [i_dont_know_blocks[i]]    
    elif "먹지" in text :
        i = randint(0,1)
        tmpblock = [i_dont_know_blocks[i]]
    else:
        i = randint(0,10)
        tmpblock = [no_blocks[i]]

    slack_web_client.chat_postMessage(
        channel = channel,
        blocks =  extract_json(tmpblock)
    )



# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"
 
if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)